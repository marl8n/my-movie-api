from unidecode import unidecode

def normalizeStr (s): unidecode(s).lower()
