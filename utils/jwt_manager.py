from typing import Optional
from jwt import decode, encode

def create_token(data: dict):
    token: str = encode(payload=data, key='secret', algorithm='HS256')
    return token

def validate_token(token: str) -> Optional[dict]:
    try:
        return decode(token, 'secret', algorithms=['HS256'])
    except:
        return None