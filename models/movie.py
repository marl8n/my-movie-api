# libraries
from sqlalchemy import Column, Float, Integer, String

# models
from config.database import Base


class Movie(Base):
    
    __tablename__ = 'movies'

    id = Column(Integer, primary_key=True)
    title = Column(String(50))
    overview = Column(String(50))
    year = Column(Integer)
    rating = Column(Float)
    category = Column(String(50))

    def __repr__(self):
        return f"Movie(title='{self.title}', description='{self.description}', trailer='{self.trailer}', year='{self.year}', rating='{self.rating}', genre_id='{self.genre_id}', director_id='{self.director_id}')"