from fastapi import APIRouter
from typing import Optional
from fastapi import Body, Depends, Path, Query, HTTPException 
from fastapi.responses import JSONResponse
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie

router = APIRouter()

@router.get('/movies', tags=["Movies"], response_model=list[Movie], dependencies=[Depends(JWTBearer())])
async def get_movies(category: Optional[str] = Query(default=None, min_length=4, max_length=15)):
    db = Session()

    if category:
        filtered_movies = MovieService(db).get_movies_by_category(category)
        return JSONResponse(content=jsonable_encoder(filtered_movies))

    # get all movies
    result = MovieService(db).get_movies()
    return JSONResponse(content=jsonable_encoder(result))

@router.get('/movies/{id}', tags=["Movies"])
async def get_movie(id: int = Path(gt=0, le=100, description="The ID of the movie to get")):

    db = Session()
    result = MovieService(db).get_movie(id)

    if not result:
        raise HTTPException(status_code=404, detail="Movie not found")

    return JSONResponse(content=jsonable_encoder(result))

@router.post('/movies', tags=["Movies"])
async def create_movie(movie: Movie = Body()):
    db = Session()
    new_movie = MovieService(db).create_movie(movie)

    return JSONResponse(status_code=201,content={"message": "Movie created"})

@router.patch('/movies/{id}', tags={"Movies"})
async def update_movie(
    id: int,
    movie: Movie = Body(default=None)
):
    db = Session()
    movie_to_update = MovieService(db).get_movie(id)
    if not movie_to_update:
        raise HTTPException(status_code=404, detail="Movie not found")

    MovieService(db).update_movie(id, movie)

    return JSONResponse(content={"message": "Movie updated", "movie": movie_to_update.id})
    


@router.delete('/movies/{id}', tags=["Movies"])
async def delete_movie(id: int):
    db = Session()
    movie_to_delete = MovieService(db).get_movie(id)
    if not movie_to_delete:
        raise HTTPException(status_code=404, detail="Movie not found")

    MovieService(db).delete_movie(movie_to_delete)
    return JSONResponse(content={"message": "Movie deleted"})