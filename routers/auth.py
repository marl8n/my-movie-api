from fastapi import Body, HTTPException
from fastapi.responses import JSONResponse

from utils.jwt_manager import create_token
from fastapi import APIRouter
from schemas.user import User

router = APIRouter()

@router.post('/login', tags=["Auth"])
async def login(user: User = Body(default=None)):
    if user.username == 'marlon' and user.password == '123':
        token: str = create_token(user.dict())
        return JSONResponse(content=token, status_code=200)

    raise HTTPException(status_code=401, detail="Unauthorized")
