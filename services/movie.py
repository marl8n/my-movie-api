from models.movie import Movie as MovieModel
from schemas.movie import Movie


class MovieService():

    def __init__(self, db) -> None:
        self.db = db
    
    def get_movies(self):
        movies = self.db.query(MovieModel).all()
        return movies

    def get_movie(self, id):
        movie = self.db.query(MovieModel).get(id)
        return movie

    def get_movies_by_category(self, category):
        movies = self.db.query(MovieModel).filter(MovieModel.category == category).all()
        return movies

    def create_movie(self, movie: Movie):
        new_movie = MovieModel(**movie.dict())
        self.db.add(new_movie)
        self.db.commit()
        return new_movie

    def update_movie(self, id, movie: Movie):
        movie_to_update = self.db.query(MovieModel).get(id)
        movie_to_update.title = movie.title if movie.title else movie_to_update.title
        movie_to_update.overview = movie.overview if movie.overview else movie_to_update.overview
        movie_to_update.category = movie.category if movie.category else movie_to_update.category
        movie_to_update.year = movie.year if movie.year else movie_to_update.year
        movie_to_update.rating = movie.rating if movie.rating else movie_to_update.rating

        self.db.commit()
        return movie_to_update

    def delete_movie(self, movie: MovieModel):
        self.db.delete(movie)
        self.db.commit()

        return movie.id