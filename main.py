from fastapi import FastAPI
from fastapi.responses import HTMLResponse

from config.database import Base, engine
from middlewares.error_handler import ErrorHandler
from routers.movie import router as router_movie
from routers.auth import router as router_auth

app = FastAPI()
app.title = 'marlon\'s project'
app.version = '0.0.1'

app.add_middleware(ErrorHandler)
app.include_router(router=router_movie)
app.include_router(router=router_auth)

Base.metadata.create_all(bind=engine)

movies = [
    {
        'id': 1,
        'title': 'Avatar',
        'overview': "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
        'year': '2009',
        'rating': 7.8,
        'category': 'Acción'    
    } 
]

@app.get('/', tags=["Home"])
async def index():
    return HTMLResponse('<h1>Hello world</h1>')