from pydantic import BaseModel, Field

class User(BaseModel):
    username: str = Field(min_length=1, max_length=50)
    password: str = Field(min_length=1, max_length=50)
    class Config:
        schema_extra = {
            'example': {
                'username': 'marlon',
                'password': '123'
            }
        }