from pydantic import BaseModel, Field 
from typing import Optional

class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(min_length=1, max_length=50)
    overview: str = Field(min_length=1, max_length=200)
    year: int = Field(gt=1900, le=2023)
    rating: float = Field(gt=0, le=10)
    category: str = Field(min_length=1, max_length=50)

    class Config:
        schema_extra = {
            'example': {
                'id': 1,
                'title': 'Oppenheimer',
                'overview': 'Boom!',
                'year': 2023,
                'rating': 9.5,
                'category': 'Drama'
            }
        }
